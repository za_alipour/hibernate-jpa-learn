package com.alipour.learn.units;

import com.alipour.learn.daos.PersonDao;
import com.alipour.learn.models.Location;
import com.alipour.learn.models.Person;
import com.alipour.learn.services.PersonService;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Arrays;
import java.util.List;

import static org.hamcrest.MatcherAssert.*;
import static org.hamcrest.Matchers.*;

public class PersonTest extends TestParentConfig {
    @Autowired
    PersonDao personDao;
    @Autowired
    PersonService personService;

    @Test
    public void add() {
        Person person = new Person();
        person.setFirstName("ali");
        person.setLastName("aliyari");
        person.setNationalCode("1245863");
        personService.add(person);
        assertThat(person.getId(), greaterThan(0L));
        assertThat(personDao.count(), equalTo(1L));

        personService.delete(person.getId());

        assertThat(personDao.count(), equalTo(0L));

    }

    @Test
    public void getByNaturalId() {
        Person person_1 = new Person();
        person_1.setFirstName("Ahmad");
        person_1.setLastName("Mohammadi");
        person_1.setNationalCode("11245");
        personService.add(person_1);
        assertThat(person_1.getId(), greaterThan(0L));

        Person person_2 = new Person();
        person_2.setFirstName("Ahmad");
        person_2.setLastName("Mohammadi");
        person_2.setNationalCode("115445");
        personService.add(person_2);
        assertThat(person_2.getId(), greaterThan(0L));

        assertThat(personService.getByNaturalId(person_2.getNationalCode()).getNationalCode(), equalTo(person_2.getNationalCode()));
        assertThat(personService.getByNaturalId(person_1.getNationalCode()).getNationalCode(), equalTo(person_1.getNationalCode()));
    }

    @Test
    public void byMultipleIds() {
        Person person_1 = new Person();
        person_1.setFirstName("Ahmad1");
        person_1.setNationalCode("1452");
        personService.add(person_1);
        assertThat(person_1.getId(), greaterThan(0L));

        Person person_2 = new Person();
        person_2.setFirstName("Ahmad2");
        person_2.setNationalCode("11885");
        personService.add(person_2);
        assertThat(person_2.getId(), greaterThan(0L));

        Person person_3 = new Person();
        person_3.setFirstName("Ahmad");
        person_3.setLastName("Mohammadi");
        person_3.setNationalCode("115445");
        personService.add(person_3);
        assertThat(person_3.getId(), greaterThan(0L));

        List<Person> persons = personService.byMultipleIds(person_2.getId(), person_3.getId());
        boolean allMatch = persons.stream().allMatch(p -> Arrays.asList(person_2.getId(), person_3.getId()).contains(p.getId()));
        assertThat(allMatch, is(true));
    }

    @Test
    public void mapOptionalProperty() {
        Person person_1 = new Person();
        person_1.setFirstName("Ahmad1");
        person_1.setNationalCode("1452");
        personService.add(person_1);
        assertThat(person_1.getId(), greaterThan(0L));

        Person person = personService.get(person_1.getId());
        assertThat(person.getLocation().isPresent(), equalTo(false));

        person_1 = new Person();
        person_1.setFirstName("Mohammad");
        person_1.setNationalCode("4781");
        person_1.setLocation(new Location(1L));
        personService.add(person_1);
        assertThat(person_1.getId(), greaterThan(0L));

        person = personService.get(person_1.getId());
        assertThat(person.getLocation().isPresent(), equalTo(true));
    }
}
