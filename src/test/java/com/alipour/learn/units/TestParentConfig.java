package com.alipour.learn.units;

import com.alipour.learn.utils.AppConfig;
import org.junit.After;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@ContextConfiguration(classes = AppConfig.class)
@RunWith(SpringJUnit4ClassRunner.class)
public class TestParentConfig{
    @Before
    public void beforeMethod(){
//        TestTransaction.flagForCommit();
//        TestTransaction.end();
    }
    @After
    public void afterMethod(){

    }

}
