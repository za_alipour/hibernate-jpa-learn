package com.alipour.learn.utils;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.*;
import org.springframework.core.env.Environment;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.JpaVendorAdapter;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;
import javax.transaction.TransactionManager;
import java.util.Properties;

@Configuration
@ComponentScan(value = {
        "com.alipour.learn.daos",
        "com.alipour.learn.services"
})
@PropertySources({
        @PropertySource("classpath:connection.properties")
})
@EnableTransactionManagement
public class AppConfig {
    @Autowired
    Environment env;

    @Bean
    public LocalContainerEntityManagerFactoryBean entityManagerFactory() {
        LocalContainerEntityManagerFactoryBean em
                = new LocalContainerEntityManagerFactoryBean();
        em.setDataSource(dataSource());
        em.setPackagesToScan("com.alipour.learn.models");

        JpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
        em.setJpaVendorAdapter(vendorAdapter);
        em.setJpaProperties(new Properties() {
            {
                setProperty("hibernate.hbm2ddl.auto", env.getProperty("db.hbm2ddl"));
                setProperty("hibernate.dialect", "org.hibernate.dialect.MySQL57Dialect");
//                setProperty("hibernate.show_sql", "true");
                setProperty("hibernate.generate_statistics", "true");//-- give additional information for time log of executed statements
                setProperty("hibernate.use_sql_comments", "true"); //-- query comments to identify a query
                setProperty("hibernate.format_sql", "true");
                setProperty("javax.persistence.sql-load-script-source", "dml/1.sql");
            }
        });

        return em;
    }

    @Bean
    public JpaTransactionManager transactionManager() {
        return new JpaTransactionManager(entityManagerFactory().getObject());
    }

    @Bean
    public DataSource dataSource() {
        DriverManagerDataSource dataSource = new DriverManagerDataSource();

        dataSource.setDriverClassName(env.getProperty("db.driver"));
        dataSource.setUsername(env.getProperty("db.username"));
        dataSource.setPassword(env.getProperty("db.password"));
        dataSource.setUrl(env.getProperty("db.url"));
        return dataSource;
    }
}
