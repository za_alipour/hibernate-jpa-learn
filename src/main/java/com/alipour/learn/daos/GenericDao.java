package com.alipour.learn.daos;

import com.alipour.learn.models.ParentEntity;

import java.util.List;

public interface GenericDao<T extends ParentEntity> {

    T get(Long id);

    T getByNaturalId(Object id);

    T getByNaturalId(String key, Object id);

    List<T> byMultipleIds(Long... ids);

    List<T> find(T t);

    List<T> getAll();

    Long count();

    T add(T t);

    T merge(T t);

    void delete(T t);

    void delete(Long id);
}
