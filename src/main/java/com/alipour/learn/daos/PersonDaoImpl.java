package com.alipour.learn.daos;

import com.alipour.learn.models.Person;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

@Repository
public class PersonDaoImpl extends GenericDaoImpl<Person> implements PersonDao {
    public PersonDaoImpl() {
        super(Person.class);
    }
}
