package com.alipour.learn.daos;

import com.alipour.learn.models.Person;

public interface PersonDao extends GenericDao<Person> {
}
