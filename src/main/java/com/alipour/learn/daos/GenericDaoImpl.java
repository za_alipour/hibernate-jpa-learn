package com.alipour.learn.daos;

import com.alipour.learn.models.ParentEntity;
import com.alipour.learn.models.ParentEntity_;
import org.hibernate.Session;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

public class GenericDaoImpl<T extends ParentEntity> implements GenericDao<T> {
    @PersistenceContext
    EntityManager entityManager;

    Class<T> persistenceClass;

    public GenericDaoImpl(Class<T> persistenceClass) {
        this.persistenceClass = persistenceClass;
    }

    public T get(Long id) {
        CriteriaBuilder builder = entityManager.getCriteriaBuilder();
        CriteriaQuery<T> query = builder.createQuery(persistenceClass);
        Root<T> from = query.from(persistenceClass);
        query.where(builder.equal(from.get(ParentEntity_.id), id));
        return entityManager.createQuery(query).getSingleResult();
    }

    @Override
    public T getByNaturalId(Object id) {
        Session session = entityManager.unwrap(Session.class);
        return session.bySimpleNaturalId(persistenceClass).load(id);
    }

    @Override
    public T getByNaturalId(String key, Object id) {
        Session session = entityManager.unwrap(Session.class);
        return session.byNaturalId(persistenceClass).using(key, id).load();
    }

    @Override
    public List<T> byMultipleIds(Long... ids) {
        Session session = entityManager.unwrap(Session.class);
        return session.byMultipleIds(persistenceClass).multiLoad(ids);
    }

    public List<T> find(T t) {
        return null;
    }

    @Override
    public List<T> getAll() {
        CriteriaBuilder builder = entityManager.getCriteriaBuilder();
        CriteriaQuery<T> query = builder.createQuery(persistenceClass);
        Root<T> from = query.from(persistenceClass);
        return entityManager.createQuery(query).getResultList();

    }

    @Override
    public Long count() {
        CriteriaBuilder builder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Long> query = builder.createQuery(Long.class);
        Root<T> from = query.from(persistenceClass);
        query.select(builder.count(from));
        return entityManager.createQuery(query).getSingleResult();
    }

    public T add(T t) {
        entityManager.persist(t);
        entityManager.flush();
        return t;
    }

    public T merge(T t) {
        entityManager.merge(t);
        entityManager.flush();
        return t;
    }

    public void delete(T t) {
        entityManager.remove(t);
        entityManager.flush();
    }

    public void delete(Long id) {
        entityManager.remove(get(id));
        entityManager.flush();
    }
}
