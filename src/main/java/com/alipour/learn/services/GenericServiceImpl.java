package com.alipour.learn.services;

import com.alipour.learn.daos.GenericDao;
import com.alipour.learn.models.ParentEntity;

import javax.transaction.Transactional;
import java.util.List;

@Transactional
public abstract class GenericServiceImpl<T extends ParentEntity> implements GenericService<T> {
    Class<T> persistenceClass;

    public GenericServiceImpl(Class<T> persistenceClass) {
        this.persistenceClass = persistenceClass;
    }

    protected abstract GenericDao<T> getDao();

    @Override
    public T get(Long id) {
        return getDao().get(id);
    }

    @Override
    public T getByNaturalId(Object id) {
        return getDao().getByNaturalId(id);
    }

    @Override
    public List<T> byMultipleIds(Long... ids) {
        return getDao().byMultipleIds(ids);
    }

    @Override
    public List<T> find(T t) {
        return getDao().find(t);
    }

    @Override
    public T add(T t) {
        return getDao().add(t);
    }

    @Override
    public T edit(T t) {
        return getDao().merge(t);
    }

    @Override
    public void delete(T t) {
        getDao().delete(t);
    }

    @Override
    public void delete(Long id) {
        getDao().delete(id);
    }
}
