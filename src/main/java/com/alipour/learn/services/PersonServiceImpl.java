package com.alipour.learn.services;

import com.alipour.learn.daos.GenericDao;
import com.alipour.learn.daos.PersonDao;
import com.alipour.learn.models.Person;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
@Transactional
public class PersonServiceImpl extends GenericServiceImpl<Person> implements PersonService {
    @Autowired
    protected PersonDao personDao;

    public PersonServiceImpl() {
        super(Person.class);
    }

    @Override
    protected GenericDao<Person> getDao() {
        return personDao;
    }
}
