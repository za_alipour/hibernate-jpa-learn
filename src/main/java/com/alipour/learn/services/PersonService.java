package com.alipour.learn.services;

import com.alipour.learn.models.Person;

public interface PersonService extends GenericService<Person> {
}
