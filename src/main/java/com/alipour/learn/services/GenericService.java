package com.alipour.learn.services;

import com.alipour.learn.models.ParentEntity;

import java.util.List;

public interface GenericService<T extends ParentEntity> {
    T get(Long id);

    T getByNaturalId(Object id);

    List<T> byMultipleIds(Long... ids);

    List<T> find(T t);

    T add(T t);

    T edit(T t);

    void delete(T t);

    void delete(Long id);
}
