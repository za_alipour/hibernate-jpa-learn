package com.alipour.learn.models;

import javax.persistence.AttributeOverride;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.Date;
import java.util.Set;

@Entity
@Table(name = "TB_PUBLICATION")
@AttributeOverride(name = "id", column = @Column(name = "PUBLICATION_ID"))
public class Publication extends ParentEntity {
    @Column(name = "VERSION", nullable = false)
    private Long version;

    @Column(name = "TITLE", nullable = false)
    private String title;

    @Column(name = "LAST_NAME")
    private Date publishingDate;


    private Set<Author> authors;

    public Long getVersion() {
        return version;
    }

    public void setVersion(Long version) {
        this.version = version;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Date getPublishingDate() {
        return publishingDate;
    }

    public void setPublishingDate(Date publishingDate) {
        this.publishingDate = publishingDate;
    }

    public Set<Author> getAuthors() {
        return authors;
    }

    public void setAuthors(Set<Author> authors) {
        this.authors = authors;
    }
}
