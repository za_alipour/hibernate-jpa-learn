package com.alipour.learn.models;

import javax.persistence.*;

@MappedSuperclass
public class ParentEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    public ParentEntity() {
    }

    public ParentEntity(Long id) {
        setId(id);
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
