package com.alipour.learn.models;

import javax.persistence.*;

@Entity
@Table(name = "TB_LOCATION")
@AttributeOverride(name = "id", column = @Column(name = "LOCATION_ID"))
public class Location extends ParentEntity {
    @Column(name = "FARSI_NAME")
    private String farsiName;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "PARENT_ID", foreignKey = @ForeignKey(name = "FK_PARENT_OF_LOCATION"))
    private Location parent;


    public Location() {
    }

    public Location(Long id) {
        super(id);
    }

    public String getFarsiName() {
        return farsiName;
    }

    public void setFarsiName(String farsiName) {
        this.farsiName = farsiName;
    }

    public Location getParent() {
        return parent;
    }

    public void setParent(Location parent) {
        this.parent = parent;
    }
}
