package com.alipour.learn.models;


import org.hibernate.annotations.NaturalId;

import javax.persistence.*;
import java.util.Optional;

@Entity
@Table(name = "TB_PERSON")
@AttributeOverride(name = "id", column = @Column(name = "PERSON_ID"))
public class Person extends ParentEntity {
    @Column(name = "FIRST_NAME", nullable = false)
    private String firstName;

    @Column(name = "LAST_NAME")
    private String lastName;

    @Column(name = "NATIONAL_CODE")
    @NaturalId
    private String nationalCode;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "LOCATION_ID", foreignKey = @ForeignKey(name = "FK_LOCATION_OF_PERSON"))
    private Location location;


    public Person() {
    }

    public Person(Long id) {
        super(id);
    }

    public Person(String firstName, String lastName, String nationalCode) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.nationalCode = nationalCode;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getNationalCode() {
        return nationalCode;
    }

    public void setNationalCode(String nationalCode) {
        this.nationalCode = nationalCode;
    }

    public Optional getLocation() {
        return Optional.ofNullable(location);
    }

    public void setLocation(Location location) {
        this.location = location;
    }
}
